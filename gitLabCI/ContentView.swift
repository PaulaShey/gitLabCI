//
//  ContentView.swift
//  gitLabCI
//
//  Created by Paulina Grużewska on 25/08/2023.
//

import SwiftUI

struct ContentView: View {
    var body: some View {
        VStack {
            Image(systemName: "fish.fill")
                .imageScale(.large)
                .foregroundColor(.accentColor)
            Text("Let the whales be whales! Leave them alone!")
        }
        .padding()
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}
