//
//  gitLabCIApp.swift
//  gitLabCI
//
//  Created by Paulina Grużewska on 25/08/2023.
//

import SwiftUI

@main
struct gitLabCIApp: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
